﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Geyser : MonoBehaviour {

    public GameObject Rabbit;
    public GameObject Wolf;

    float mass = 3.0F; // defines the character mass
    Vector3 impact = Vector3.zero;

    public float FuerzaRabbit;
    public float FuerzaWolf;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


        /*
        if (impact.magnitude > 0.2F) Wolf.GetComponent<CharacterController>().Move(impact * Time.deltaTime);
        // consumes the impact energy each cycle:
        impact = Vector3.Lerp(impact, Vector3.zero, 5 * Time.deltaTime);

        if (impact.magnitude > 0.2F) Rabbit.transform.GetChild(3).GetComponent<CharacterController>().Move(impact * Time.deltaTime);
        // consumes the impact energy each cycle:
        impact = Vector3.Lerp(impact, Vector3.zero, 5 * Time.deltaTime);
        */
    }
    /*
    void OnTriggerStay(Collider collision)
    {

        //check if the object we are colliding with is a car
        if (collision.tag == "Rabbit")
        {
            AddImpact(new Vector3(0, 1, 0), FuerzaRabbit);
            //if so, do something... like adding a force to the right
            // Rabbit.GetComponent<Transform>().localPosition += new Vector3(0, Fuerza, 0) * Time.deltaTime;
        }
        if (collision.tag == "Wolf")
        {
            Vector3 finalDirection = Wolf.transform.forward*0.5f;
            finalDirection.y = 0.8f;
            Debug.Log(finalDirection);
            AddImpact(finalDirection, FuerzaWolf);
            //if so, do something... like adding a force to the right
           // Wolf.GetComponent<Transform>().localPosition += new Vector3(0, Fuerza, 0) * Time.deltaTime;
        }
    }*/

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("ea");
      if (collision.gameObject.tag == "Rabbit")
      {
            gameObject.GetComponent<ParticleSystem>().Play();
        }
        if (collision.gameObject.tag == "Wolf")
        {
            gameObject.GetComponent<ParticleSystem>().Play();
        }
        else
        {
       //     gameObject.GetComponent<ParticleSystem>().Stop();
        }
    }
   void OnCollisionExit(Collision collision)
    {

        gameObject.GetComponent<ParticleSystem>().Stop();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Rabbit" || other.tag == "Wolf")
        {
            gameObject.GetComponent<ParticleSystem>().Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Rabbit" || other.tag == "Wolf")
        {
            gameObject.GetComponent<ParticleSystem>().Stop();
        }
    }



    /*
    public void AddImpact(Vector3 dir, float force)
    {
        dir.Normalize();
        if (dir.y < 0) dir.y = -dir.y; // reflect down force on the ground
        impact += dir.normalized * force / mass;
    }*/
}
