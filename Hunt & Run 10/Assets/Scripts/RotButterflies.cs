﻿using UnityEngine;
using System.Collections;

public class RotButterflies : MonoBehaviour {

	public float speed;
	Vector3 objectPos;

	public float range = 50;

	Vector3 target;

	public float moveSpeed;
	public float waittime;

	// Use this for initialization
	void Start () {
		//transform.localEulerAngles += new Vector3 (0.1f, 0.0f, 0.0f);
		objectPos = transform.position;
		StartCoroutine (ChangeTarget ());
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.up * Time.deltaTime * speed, Space.World);
		transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * moveSpeed);
		//localEulerAngles += new Vector3 (0.0f, 1.0f, 0.0f);
	}


	IEnumerator ChangeTarget()
	{
		target = Random.insideUnitSphere * range + objectPos;
		yield return new WaitForSeconds (waittime);
		StartCoroutine (ChangeTarget ());

	}
}
