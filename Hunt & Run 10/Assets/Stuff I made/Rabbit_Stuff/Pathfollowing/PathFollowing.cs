﻿using UnityEngine;
using System.Collections;

public class PathFollowing : MonoBehaviour {

    public Transform[] path;
    public float speed = 5.0f;
    public float reachDist = 1.0f;
    public int currentPoint = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        /*
        //Version 1 con lerp
        Vector3 dir = path[currentPoint].position - transform.position;
        transform.position += dir * Time.deltaTime * speed;

        if (dir.magnitude <= reachDist)
        {
            currentPoint++;
        }

        if (currentpoint >= path.Length)
        {
            //Finalizar pathfolowing
            currentPoint = 0;
        }*/


        //Version 2 sin lerp
        float dist = Vector3.Distance(path[currentPoint].position, transform.position);

        transform.position = Vector3.MoveTowards(transform.position, path[currentPoint].position, Time.deltaTime * speed);

        if (dist <= reachDist)
        {
            currentPoint++;
        }

        if (currentPoint >= path.Length)
        {
            //Finalizar pathfollowing
            currentPoint = 0;

        }

	
	}

   /* void OnDrawGizmos()
    {
        for (int i = 0; i < path.Length; i++)
        {
            if (path[i] != null)
            {

            }
        }
    }*/
}
