﻿using UnityEngine;
using System.Collections;

public class Rabbit_Controller : MonoBehaviour {

    public int health = 2;

    public GameObject kitten;


    public Transform[] path;
    public float speed = 5.0f;
    public float reachDist = 1.0f;
    public int currentPoint = 0;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "wolfattack")
        {
            if (health == 2)
            {
                TakeDamage();
            }
            else if (health == 1)
            {
                Death();
            }

        }
        else if (collider.tag == "carrot")
        {
            Destroy(collider.gameObject);
            PickUpCarrot();
        }
        else if (collider.tag == "tunnel")
        {
            path = collider.gameObject.GetComponent<Tunnel_Script>().path;
            StartCoroutine(TakeTunnel());
        }
    }

    public void TakeDamage()
    {
        health--;
    }

    public void PickUpCarrot()
    {
        health++;
    }

    public void PickUpKitten()
    {
        if (kitten.activeSelf == true)
        {

        }
        else
        {
            kitten.SetActive(true);
        }
    }

    public void DeliverKitten()
    {
        kitten.SetActive(false);
    }

    public void Death()
    {
        Destroy(gameObject);
    }

    IEnumerator TakeTunnel()
    {
        //Disable controls

        //Disable Hitboxes

        //Reset the point
        currentPoint = 0;

        while (currentPoint < path.Length)
        {
            float dist = Vector3.Distance(path[currentPoint].position, transform.position);

            transform.position = Vector3.MoveTowards(transform.position, path[currentPoint].position, Time.deltaTime * speed);

            if (dist <= reachDist)
            {
                currentPoint++;
            }

            yield return new WaitForSeconds(0.01f);
        }    
    }

}
