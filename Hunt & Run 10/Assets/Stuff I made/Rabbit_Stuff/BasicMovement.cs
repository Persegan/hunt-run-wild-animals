﻿using UnityEngine;
using System.Collections;

public class BasicMovement : Photon.PunBehaviour
{

    public float speed = 10.0f;
    public float rotationSpeed = 2.0f;

    public float mouseRotationSpeed = 2.0f;

    public float gravity = 30.0f;

    public float maxY = 80;
    public float minY = 80;
    private float currentY = 0;

    public GameObject Rotator;
    public GameObject BaseRotator;

    private float yVelocity = 0.0f;
    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
            Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
            Vector2 mouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

            Vector3 auxiliar = Rotator.transform.localEulerAngles + new Vector3(-mouseInput.y * mouseRotationSpeed, mouseInput.x * mouseRotationSpeed, 0);
            if (auxiliar.x < 0)
        {
            auxiliar.x = 0;
        }
        Rotator.transform.localEulerAngles = auxiliar;
            //Rotator.transform.localEulerAngles = new Vector3(Mathf.Clamp(Rotator.transform.localEulerAngles.x, minY, maxY), Rotator.transform.localEulerAngles.y, 0);

            
           if (Rotator.transform.localEulerAngles.x > maxY)
            {
                Rotator.transform.localEulerAngles = new Vector3(maxY, Rotator.transform.localEulerAngles.y, 0);
            }

            if (Rotator.transform.localEulerAngles.x < minY)
            {
                
                Rotator.transform.localEulerAngles = new Vector3(minY, Rotator.transform.localEulerAngles.y, 0);
            }


            BaseRotator.transform.localEulerAngles += new Vector3(0, mouseInput.x * mouseRotationSpeed, 0);

            yVelocity -= gravity * Time.deltaTime;
            if (GetComponent<CharacterController>().isGrounded)
            {
                yVelocity = 0;
            }

            gameObject.GetComponent<CharacterController>().Move(BaseRotator.transform.TransformDirection(input.normalized * speed * Time.deltaTime + yVelocity * Vector3.up * Time.deltaTime));

        if (input != Vector3.zero)
        {
            transform.localRotation = Quaternion.Slerp(
                transform.localRotation,
                Quaternion.LookRotation(BaseRotator.transform.TransformDirection(input)),
                Time.deltaTime * rotationSpeed
            );
            GetComponent<Animator>().SetBool("isWalking", true);
        }
        else
        {
            GetComponent<Animator>().SetBool("isWalking", false);
        }
       
    }

}