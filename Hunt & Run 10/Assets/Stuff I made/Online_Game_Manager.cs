﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using System;

public class Online_Game_Manager : Photon.PunBehaviour, IPunCallbacks, IPunObservable{

    public static int round = 1;
    public Vector3 [] carrot_positions;

    public List <GameObject> carrots;

	// Use this for initialization
	void Start () {
        round = 1;

        if (Player_Status.Player_Number == 1)
        {			
            GameObject temporal =  PhotonNetwork.Instantiate("Wolf_Controller", new Vector3(29, 140, 60), Quaternion.identity, 0);
			temporal.transform.GetChild(1).GetComponent<Camera>().enabled = true;
            temporal.transform.GetChild(1).GetComponent<AudioListener>().enabled = true;
            temporal.GetComponent<FirstPersonController_Online_Version>().owner = true;
            temporal.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
            temporal.GetComponent<FirstPersonController_Online_Version>().step_sound.volume = 0.1f;

            for (int i = 0; i < carrot_positions.Length; i++)
            {
                carrots.Add(PhotonNetwork.Instantiate("Zanahoria", carrot_positions[i], Quaternion.identity, 0));
            }
        }
        else
        {//PhotonNetwork.Destroy(GameObject.Find("FirstPersonCharacter"));
            GameObject temporal = PhotonNetwork.Instantiate("Rabbit", new Vector3(29, 150, 60), Quaternion.identity, 0);
            temporal.transform.GetChild(2).gameObject.SetActive(true);
            temporal.GetComponentInChildren<BasicMovement>().enabled = true;
            temporal.GetComponentInChildren<CharacterController>().enabled = true;
            temporal.transform.GetChild(3).GetChild(0).gameObject.GetComponent<Renderer>().material.SetFloat("_Outline", 0.0f);
            temporal.transform.GetChild(3).GetComponent<Rabbit_Online_Manager>().step_sound.volume = 0.1f;
        }
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("Photon_Launcher");
    }

    public override void OnDisconnectedFromPhoton()
    {
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("Photon_Launcher");
    }


    [PunRPC]
    public void EndRound()
    {
        StartCoroutine(End_Round_Coroutine());
    }


    [PunRPC]
    public void increase_player_1_score()
    {
        Player_Status.Player1_Score++;
    }


    [PunRPC]
    public void increase_player_2_score()
    {
        Player_Status.Player2_Score++;
    }

    [PunRPC]
    public void DestroyObject(int viewID)
    {
        PhotonView auxiliar = PhotonView.Find(viewID);
        if (auxiliar.isMine)
        {
            PhotonNetwork.Destroy(auxiliar.gameObject);
        }
    }



    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new NotImplementedException();
    }

    IEnumerator End_Round_Coroutine()
    {
        if (Online_Game_Manager.round == 2)
        {
            GameObject temporal = GameObject.FindGameObjectWithTag("Wolf");
            temporal.GetComponent<Timer>().enabled = false;
            temporal.transform.GetChild(1).GetChild(0).GetChild(5).gameObject.SetActive(false);
            temporal.transform.GetChild(1).GetChild(0).GetChild(6).gameObject.SetActive(false);
            temporal.transform.GetChild(1).GetChild(0).GetChild(7).gameObject.SetActive(false);
            if (temporal.GetPhotonView().isMine == true)
            {
                temporal.GetComponent<FirstPersonController_Online_Version>().owner = false;
                temporal.transform.GetChild(1).GetChild(0).GetChild(8).gameObject.SetActive(true);
                temporal.transform.GetChild(0).GetComponent<Animator>().SetBool("isWalking", false);
                yield return new WaitForSeconds(10.0f);
                //PhotonNetwork.Destroy(temporal);
                PhotonNetwork.Disconnect();
                SceneManager.LoadScene("Photon_Launcher");
            }

            temporal = GameObject.FindGameObjectWithTag("Rabbit_Base");
            if (temporal.GetPhotonView().isMine == true)
            {
                temporal.GetComponentInChildren<BasicMovement>().enabled = false;
                temporal.transform.GetChild(0).GetChild(4).gameObject.SetActive(true);
                temporal.transform.GetChild(3).GetComponent<Animator>().SetBool("isWalking", false);
                temporal.GetComponent<Animator>().enabled = false;
                yield return new WaitForSeconds(10.0f);
                //PhotonNetwork.Destroy(temporal);
                PhotonNetwork.Disconnect();
                SceneManager.LoadScene("Photon_Launcher");
            }
 
        }
        else
        {
            GameObject temporal = GameObject.FindGameObjectWithTag("Wolf");
            temporal.GetComponent<Timer>().enabled = false;
            temporal.transform.GetChild(1).GetChild(0).GetChild(5).gameObject.SetActive(false);
            temporal.transform.GetChild(1).GetChild(0).GetChild(6).gameObject.SetActive(false);
            temporal.transform.GetChild(1).GetChild(0).GetChild(7).gameObject.SetActive(false);
            temporal.transform.GetChild(0).GetComponent<Animator>().SetBool("isWalking", false);

            if (Player_Status.Player_Number == 1)
            {                
                if (temporal.GetPhotonView().isMine == true)
                {
                    temporal.GetComponent<FirstPersonController_Online_Version>().owner = false;
                    temporal.GetComponent<Timer>().enabled = false;
                    temporal.transform.GetChild(1).GetChild(0).GetChild(8).gameObject.SetActive(true);
                    yield return new WaitForSeconds(10.0f);
                    PhotonNetwork.Destroy(temporal);
                }

                temporal = PhotonNetwork.Instantiate("Rabbit", new Vector3(29, 150, 60), Quaternion.identity, 0);
                temporal.GetComponentInChildren<BasicMovement>().enabled = true;
                temporal.GetComponentInChildren<CharacterController>().enabled = true;
                temporal.transform.GetChild(2).gameObject.SetActive(true);
                temporal.transform.GetChild(3).GetChild(0).gameObject.GetComponent<Renderer>().material.SetFloat("_Outline", 0.0f);
                temporal.transform.GetChild(3).GetComponent<Rabbit_Online_Manager>().step_sound.volume = 0.1f;
                for (int i = 0; i < carrots.Count; i++)
                {
                    if (carrots[i] != null)
                    PhotonNetwork.Destroy(carrots[i]);
                }
                for (int i = 0; i < carrot_positions.Length; i++)
                {
                    carrots.Add(PhotonNetwork.Instantiate("Zanahoria", carrot_positions[i], Quaternion.identity, 0));
                }
            }
            else
            {
               temporal = GameObject.FindGameObjectWithTag("Rabbit_Base");
                if (temporal.GetPhotonView().isMine == true)
                {
                    temporal.GetComponentInChildren<BasicMovement>().enabled = false;
                    temporal.transform.GetChild(0).GetChild(4).gameObject.SetActive(true);
                    temporal.transform.GetChild(3).GetComponent<Animator>().enabled = false;
                    temporal.transform.GetChild(3).GetComponent<Animator>().SetBool("isWalking", false);
                    yield return new WaitForSeconds(10.0f);
                    PhotonNetwork.Destroy(temporal);
                }

                temporal = PhotonNetwork.Instantiate("Wolf_Controller", new Vector3(29, 140, 60), Quaternion.identity, 0);
                temporal.transform.GetChild(1).GetComponent<Camera>().enabled = true;
                temporal.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
                temporal.transform.GetChild(1).GetComponent<AudioListener>().enabled = true;
                temporal.GetComponent<FirstPersonController_Online_Version>().owner = true;
                temporal.GetComponent<FirstPersonController_Online_Version>().step_sound.volume = 0.1f;
            }
        }
        round++;
        Debug.Log("THE ROUND IS " + round);
    }
}
