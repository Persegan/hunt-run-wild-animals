﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soundtrack_Manager : MonoBehaviour {

    public AudioSource calm_song;
    public AudioSource action_song;

    public float distance;


    private GameObject rabbit;
    private GameObject wolf;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (rabbit == null)
            rabbit = GameObject.FindGameObjectWithTag("Rabbit");

        if (wolf == null)
            wolf = GameObject.FindGameObjectWithTag("Wolf");   

        if (rabbit != null && wolf != null)
        {
            if (Vector3.Distance(rabbit.transform.position, wolf.transform.position) <= distance)
            {
                calm_song.volume = 0.0f;
                action_song.volume = 0.05f;
            }
            else
            {
                calm_song.volume = 0.05f;
                action_song.volume = 0.0f;
            }
        }
	}
}
