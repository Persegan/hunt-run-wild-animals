﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class Rabbit_Online_Manager : Photon.PunBehaviour, IPunObservable {

    public AudioSource carrot_pickup_sound;
    public AudioSource step_sound;
    public GameObject attacker;
    bool changeround = true;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // We own this player: send the others our data
            //stream.SendNext(IsFiring);
        }
        else
        {
            // Network player, receive data
            //this.IsFiring = (bool)stream.ReceiveNext();
        }
    }

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {

        /*if (transform.GetComponentInParent<Timer>().t <= 0)
        {
            gameObject.GetComponent<PhotonView>().RPC("EndRound", PhotonTargets.AllViaServer);
            EndRound();
        }*/
	
	}


    private void OnTriggerEnter(Collider other)
    {
        if (photonView.isMine == false)
        {
            if (other.tag == "Wolf_Attack")
            {
                if (changeround == true)
                {
                    changeround = false;
                    StartCoroutine(round_change_Safe());
                    attacker = other.gameObject;
                    GameObject.FindGameObjectWithTag("Online_Game_Manager").GetComponent<Online_Game_Manager>().GetComponent<PhotonView>().RPC("EndRound", PhotonTargets.AllViaServer);
                    //gameObject.GetComponent<PhotonView>().RPC("EndRound", PhotonTargets.AllViaServer);
                }
            }            
        }
        if (photonView.isMine == true)
        {
            if (other.tag == "Zanahoria")
            {
                GetComponent<PhotonView>().RPC("make_carrot_sound", PhotonTargets.All);
                GameObject.FindGameObjectWithTag("Online_Game_Manager").GetComponent<Online_Game_Manager>().GetComponent<PhotonView>().RPC("DestroyObject", PhotonTargets.AllViaServer, other.gameObject.GetComponent<PhotonView>().viewID);
                if (Player_Status.Player_Number == 1)
                {
                    GameObject.FindGameObjectWithTag("Online_Game_Manager").GetComponent<Online_Game_Manager>().GetComponent<PhotonView>().RPC("increase_player_1_score", PhotonTargets.AllViaServer);
                }
                else if (Player_Status.Player_Number == 2)
                {
                    GameObject.FindGameObjectWithTag("Online_Game_Manager").GetComponent<Online_Game_Manager>().GetComponent<PhotonView>().RPC("increase_player_2_score", PhotonTargets.AllViaServer);
                }
            }
        }

    }


    /*[PunRPC]
    public void EndRound()
    {      
        if (Online_Game_Manager.round == 2)
        {
            PhotonNetwork.Disconnect();
            SceneManager.LoadScene("Photon_Launcher");
        }
        else
        {
            GameObject temporal = PhotonNetwork.Instantiate("Wolf_Controller", new Vector3(50, 15, 37), Quaternion.identity, 0);
            temporal.transform.GetChild(1).gameObject.SetActive(true);
            temporal.GetComponent<FirstPersonController_Online_Version>().owner = true;
        }
    }
    */



    IEnumerator round_change_Safe()
    {
        changeround = false;
        yield return new WaitForSeconds(3.0f);
        changeround = true;
    }

    [PunRPC]
    public void make_carrot_sound()
    {
        carrot_pickup_sound.Play();
    }

    public void make_step_sound()
    {
        GetComponent<PhotonView>().RPC("make_step_sound_RPC", PhotonTargets.All);
    }

    [PunRPC]
    public void make_step_sound_RPC()
    {
        step_sound.Play();
    }

    public void stop_step_sound()
    {
        GetComponent<PhotonView>().RPC("stop_step_sound_RPC", PhotonTargets.All);
    }

    [PunRPC]
    public void stop_step_sound_RPC()
    {
        step_sound.Stop();
    }
}
