﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Hidden/GPUGemBlur"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}
		SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler _CameraDepthTexture;
			sampler2D _MainTex;
			uniform float4x4 prevViewProjectionMatrix;
			uniform float4x4 currentInvsViewProj;
			uniform float4x4 currentViewProj;
			uniform int numSamples;
			uniform float _Strength;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv.xy = v.uv;
				return o;
			}

			//I thank user DBN on the forum thread http://answers.unity3d.com/questions/218333/shader-inversefloat4x4-function.html for the inverse function.
			/*float4x4 inverse(float4x4 input)
			{
				#define minor(a,b,c) determinant(float3x3(input.a, input.b, input.c))
				//determinant(float3x3(input._22_23_23, input._32_33_34, input._42_43_44))
					float4x4 cofactors = float4x4(
						minor(_22_23_24, _32_33_34, _42_43_44),
						-minor(_21_23_24, _31_33_34, _41_43_44),
						minor(_21_22_24, _31_32_34, _41_42_44),
						-minor(_21_22_23, _31_32_33, _41_42_43),

						-minor(_12_13_14, _32_33_34, _42_43_44),
						minor(_11_13_14, _31_33_34, _41_43_44),
						-minor(_11_12_14, _31_32_34, _41_42_44),
						minor(_11_12_13, _31_32_33, _41_42_43),

						minor(_12_13_14, _22_23_24, _42_43_44),
						-minor(_11_13_14, _21_23_24, _41_43_44),
						minor(_11_12_14, _21_22_24, _41_42_44),
						-minor(_11_12_13, _21_22_23, _41_42_43),
						-minor(_12_13_14, _22_23_24, _32_33_34),
						minor(_11_13_14, _21_23_24, _31_33_34),
						-minor(_11_12_14, _21_22_24, _31_32_34),
						minor(_11_12_13, _21_22_23, _31_32_33));
				#undef minor
				return transpose(cofactors) / determinant(input);
			}*/



			fixed4 frag(v2f i) : SV_Target
			{
				#define sceneSampler _MainTex 
				#define texCoord i.uv
				//Comments are from the book CPU Gems 3.

				//Get the depth buffer value at this pixel.
				float zOverW = tex2D(_CameraDepthTexture, texCoord);
				// H is the viewport position at this pixel in the range -1 to 1.
				float4 H = float4(texCoord.x * 2 - 1, (1 - texCoord.y) * 2 - 1, zOverW, 1);
				//Transform by the view-projection inverse.	//float4x4 inVerseViewProjectionMatrix = inverse(UNITY_MATRIX_VP);	//float4x4 inVerseViewProjectionMatrix = unity_CameraInvProjection;
				float4 D = mul(H, currentInvsViewProj);
				//Divide by w to get the world position.
				float4 worldPos = D / D.w;

				//Current viewPort potision
				float4 currentPos = H;
				//Use the world position, adn tranform by the previous viev-projection matrix.
				float4 previousPos = mul(worldPos, prevViewProjectionMatrix);
				//Convert to nonhomogeneous points [-1,1] by dividing by w.
				previousPos /= previousPos.w;
				//Use this frame's position and last frame's to compute the pixel velocity.
				float2 velocity = (currentPos - previousPos) / _Strength;

				//Get the initial color at this pixel.
				float4 color = tex2D(sceneSampler, texCoord);
				texCoord += velocity;
				for (int loops = 1; loops < numSamples; ++loops, texCoord += velocity) {
					//Sample the color buffer along the velocity vector.
					float4 currentColor = tex2D(sceneSampler, texCoord);
					//Add the current color to out color sum.
					color += currentColor;
				}
				//Average all of the samples to get the final blur color.
				float4 finalColor = color / numSamples;
				//float4 finalColor = 
				return finalColor;
				#undef sceneSampler
				#undef texCoord
			}
			ENDCG
		}
	}
}
