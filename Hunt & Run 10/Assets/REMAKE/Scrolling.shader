﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/TextureRotation"
{
	Properties
	{
		_Diffuse("_Diffuse", 2D) = "black" {}
	}

		SubShader
	{
		Pass
	{
		CGPROGRAM

#pragma vertex vert
#pragma fragment frag
#pragma target 2.0          

		sampler2D _Diffuse;
	float4x4 _Rotation;
	uniform float4 _Diffuse_ST;

	struct vertexInput
	{
		float4 vertex : POSITION;
		float4 texcoord : TEXCOORD0;
	};

	struct vertexOutput
	{
		float4 pos : SV_POSITION;
		half2 col : TEXCOORD0;
	};

	struct Input
	{
		float2 uv_Diffuse;
	};

	vertexOutput vert(vertexInput input)
	{
		vertexOutput output;

		output.pos = UnityObjectToClipPos(input.vertex);
		output.col = input.texcoord.xy *_Diffuse_ST.xy + _Diffuse_ST.zw;
		return output;
	}

	float4 frag(vertexOutput IN) : COLOR
	{
		float4 MxV0 = mul(_Rotation, (IN.col.xyxy));
		return tex2D(_Diffuse,MxV0.xy);
	}
		ENDCG
	}
	}
}