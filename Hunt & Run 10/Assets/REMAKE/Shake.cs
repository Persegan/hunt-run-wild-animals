﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour {

    public float shakeVel = 0.5f;
    float minVal = 0.0f;
    float maxVal = 1.0f;
    float currVal = 0.5f;
    bool scalator = false;
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (scalator) currVal = currVal + shakeVel*0.1f;
        else currVal = currVal - shakeVel * 0.1f;

        if (scalator) this.transform.localScale += new Vector3(0.0015f,0.0f,0.0f);
        if (!scalator) this.transform.localScale -= new Vector3(0.0015f, 0.0f, 0.0f);

        if (currVal >= maxVal) scalator = false;
        if (currVal <= minVal) scalator = true;

        

    }
}
