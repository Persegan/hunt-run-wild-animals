﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardTREES : MonoBehaviour {
    public Camera m_Camera;

    private void Start()
    {
        m_Camera = Camera.main;
    }

    

	void Update()
	{
        m_Camera = Camera.main;
        transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
			m_Camera.transform.rotation * Vector3.up);
	}
}
