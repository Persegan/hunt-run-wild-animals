﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets.CrossPlatformInput;

public class Mordedura : MonoBehaviour {

	public bool Top;
	public bool Bottom;
    // Use this for initialization
    public Renderer rend;

    public GameObject Mouth;

    Vector3 topPos;
    Vector3 botPos;

    void Start () {
        topPos = new Vector3(89.53571f, -43.09499f, -344.7304f);
        botPos = new Vector3(89.53571f, -43.75399f, -344.7304f);
    }
	
	// Update is called once per frame
	void Update () {
        rend = GetComponent<Renderer>();
        rend.enabled = false;
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<Light>().enabled = false;

        if (CrossPlatformInputManager.GetButtonDown ("Fire1")) {

//            StartCoroutine("DestroyThis",1);

            if (Top) {
                StartCoroutine("MoveDOWN");
			}
            else if(Bottom) StartCoroutine("MoveUP");
        }
	}

	IEnumerator MoveUP(){
        rend = GetComponent<Renderer>();
        rend.enabled = true;

        GetComponent<BoxCollider>().enabled = true;
        GetComponent<Light>().enabled = true;

        for (float i = 0; i <= 1; i += 0.1f) {
            transform.Translate(Vector3.up * Time.deltaTime, Space.World);
            if (i == 1)
            {
                transform.position = botPos;
            }
        }
        yield return null;
    }
    IEnumerator MoveDOWN()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = true;

        GetComponent<BoxCollider>().enabled = true;
        GetComponent<Light>().enabled = true;
        for (float i = 0; i <= 1; i += 0.1f)
        {
            transform.Translate(Vector3.down * Time.deltaTime, Space.World);
            if (i == 1)
            {
                transform.position = topPos;
            }
        }
        yield return null;
    }
    IEnumerator DestroyThis(float time)
    {
        yield return new WaitForSeconds(time);

        Object.Destroy(this.transform.parent.gameObject);
    }
    IEnumerator Instantiate(float time)
    {
        yield return new WaitForSeconds(time);

      //  GameObject temporal = Instantiate(Mouth, transform.parent.transform.parent.transform.position + mouthPos, Quaternion.Euler(mouthRot) , transform.parent.transform.parent);
        //temporal.transform.localPosition = mouthPos;

    }
}
