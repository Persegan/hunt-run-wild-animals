﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class auxiliar_wolf_animation_script : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void make_step_sound()
    {
        transform.GetComponentInParent<FirstPersonController_Online_Version>().make_step_sound();
    }

    public void stop_step_sound()
    {
        transform.GetComponentInParent<FirstPersonController_Online_Version>().stop_step_sound();
    }
}
