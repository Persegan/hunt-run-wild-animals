﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayersScore : MonoBehaviour {

	public GameObject Score;

	public Text player1Text;
	public Text player2Text;
	private float startScore;



	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

        /*if (Input.GetKey (KeyCode.Tab)) {
			Score.SetActive (true);
		}
		else
			Score.SetActive (false);*/
		    
        if (Player_Status.Player_Number == 1)
        {
            player1Text.text = "Player 1 (You)" + ":    " + Player_Status.Player1_Score;
        }
        else
        {
            player1Text.text = "Player 1" + ":    " + Player_Status.Player1_Score;
        }

        if (Player_Status.Player_Number == 2)
        {
            player2Text.text = "Player 2 (You)" + ":    " + Player_Status.Player2_Score;
        }
        else
        {
            player2Text.text = "Player 2" + ":    " + Player_Status.Player2_Score;
        }
		
	}
}
