﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

	public Text timerText;
	public Text timerText2;
	private float startTimer;
    public float t;

    private bool auxiliar_bool = true;

	void Start () {
        //startTimer = Time.time;
        auxiliar_bool = true;
	}
	

	// Update is called once per frame
	void Update () {
        if (auxiliar_bool == true)
        t -= Time.deltaTime;
		if (t >= 0) {
			string minutes = ((int)t / 60).ToString ();
			string seconds = (t % 60).ToString ("f2");

			timerText.text = minutes + ":" + seconds;
			timerText2.text = minutes + ":" + seconds;
		}
        else
        { 
            if (auxiliar_bool == true)
            {
                GameObject.FindGameObjectWithTag("Online_Game_Manager").GetComponent<Online_Game_Manager>().GetComponent<PhotonView>().RPC("EndRound", PhotonTargets.AllViaServer);
                auxiliar_bool = false;
            }
            
        }
	}
}
